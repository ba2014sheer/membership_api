#!/bin/sh
set -ef

##
# Build variables
##
if [ -z "$REGISTRY" ]; then
    echo "Missing environment variable REGISTRY from .gitlab.ci.yml"
    exit 1
fi
if [ -z "$TAG" ]; then
    echo "Missing environment variable TAG from .gitlab.ci.yml"
    exit 1
fi
if [ -z "$ENV_NAME" ]; then
    echo "Missing environment variable ENV_NAME from .gitlab.ci.yml"
    exit 1
fi
if [ -z "$NAMESPACE" ]; then
    NAMESPACE="dsasanfrancisco"
fi

##
# Push protected branches / tags to DockerHub
##
case "$ENV_NAME" in
    # Supporting environment names that match the protected branch / tag name
    dev|prod)
        # For now the only major difference between dev and prod is the domain pattern
        if [ "$ENV_NAME" = "prod" ]; then
            DOMAIN="api.dsasf.org"
        else
            DOMAIN="api.$ENV_NAME.dsasf.org"
        fi
        # Everything after this is the same for prod and dev environments
        echo "Deploying to $DOMAIN..."
        echo "Setting up SSH..."
        set -x
        apk update
        apk add openssh
        mkdir -p /root/.ssh
        set +x # hide command
        echo "$DEPLOY_PRIVATE_KEY" > /root/.ssh/id_rsa
        set -x
        chmod 400 /root/.ssh/id_rsa
        echo "Logging in to hub.docker.com with '$DOCKERHUB_USER'..."
        set +x # hide command
        # Pull the membership_api from the registry
        docker login -u "$DOCKERHUB_USER" -p "$DOCKERHUB_PASSWORD"
        echo "Deploying $NAMESPACE:$TAG to '$ENV_NAME'..."
        set -x
        docker pull "$REGISTRY/$NAMESPACE/membership_api:$TAG"
        docker pull "$REGISTRY/$NAMESPACE/membership_api/migrate:$TAG"
        # If the environment name is different than the tag, then push both
        if [ "$TAG" != "$ENV_NAME" ]; then
            docker tag "$REGISTRY/$NAMESPACE/membership_api:$TAG" "$NAMESPACE/membership_api:$TAG"
            docker push "$NAMESPACE/membership_api:$TAG"
            docker tag "$REGISTRY/$NAMESPACE/membership_api/migrate:$TAG" "$NAMESPACE/membership_migrate:$TAG"
            docker push "$NAMESPACE/membership_migrate:$TAG"
        fi
        # Tag as the environment name as well
        docker tag "$REGISTRY/$NAMESPACE/membership_api:$TAG" "$NAMESPACE/membership_api:$ENV_NAME"
        docker push "$NAMESPACE/membership_api:$ENV_NAME"
        docker tag "$REGISTRY/$NAMESPACE/membership_api/migrate:$TAG" "$NAMESPACE/membership_migrate:$ENV_NAME"
        docker push "$NAMESPACE/membership_migrate:$ENV_NAME"

        # SSH to the host and restart systemd to pickup the new tagged images
        ssh-keygen -R ${DOMAIN} || true
        set +x # hide command
        ssh -oStrictHostKeyChecking=no deploy@${DOMAIN} \
            "sudo ENV_NAME=$ENV_NAME TAG=$TAG DOCKERHUB_USER='$DOCKERHUB_USER' DOCKERHUB_PASSWORD='$DOCKERHUB_PASSWORD' /root/upgrade-api.sh"
        set -x

        # If the deploy is successful on prod, tag it as latest and push it to dockerhub.io
        if [ "$ENV_NAME" = "prod" ]; then
            docker tag "$REGISTRY/$NAMESPACE/membership_api:$TAG" "$NAMESPACE/membership_api:latest"
            docker push "$NAMESPACE/membership_api:latest"
            docker tag "$REGISTRY/$NAMESPACE/membership_api/migrate:$TAG" "$NAMESPACE/membership_migrate:latest"
            docker push "$NAMESPACE/membership_migrate:latest"
        fi
        ;;
    # Otherwise
    default)
        echo "Unsupported CD environment '$ENV_NAME', nothing to deploy."
        ;;
esac
