from werkzeug.test import Client
from werkzeug.wrappers import BaseResponse
from config import SUPER_USER_EMAIL
from datetime import datetime

from membership.database.base import engine, metadata, Session
from membership.database.models import \
    Attendee, Committee, Meeting, Member, NationalMembershipData, Role, \
    Election, Chapter, InterestTopic, ProxyTokenState, ProxyToken, EligibleVoter, Interest, \
    PhoneNumber, Identity
from membership.web.base_app import app
from tests.flask_utils import post_json


class TestMergeMembers:
    def setup(self):
        metadata.drop_all(engine)
        metadata.create_all(engine)
        self.app = Client(app, BaseResponse)
        self.app = app.test_client()
        self.app.testing = True

        session = Session()

        # Setup for Auth
        m = Member()
        m.email_address = SUPER_USER_EMAIL
        session.add(m)
        role1 = Role(member=m, role='member', date_created=datetime(2016, 1, 1))
        role2 = Role(member=m, role='admin', date_created=datetime(2016, 1, 1))
        session.add_all([role1, role2])
        membership = NationalMembershipData(
            member=m,
            ak_id='73750',
            first_name='Huey',
            last_name='Newton',
            address_line_1='123 Main St',
            city='Oakland',
            country='United States',
            zipcode='94612',
            dues_paid_until=datetime(2020, 6, 1, 0),
        )
        session.add(membership)

        # Setup foreign key dependencies
        session.add(Election(
            id=509329, name="vote a", status="status", number_winners=1,
            voting_begins_epoch_millis=1010101, voting_ends_epoch_millis=10103801,
            description="desc", description_img="/img", author="author"))
        session.add(Election(
            id=62829, name="vote b", status="status", number_winners=1,
            voting_begins_epoch_millis=1010101, voting_ends_epoch_millis=10103801,
            description="desc", description_img="/img", author="author"))
        session.add(Election(
            id=72920, name="vote c", status="status", number_winners=1,
            voting_begins_epoch_millis=1010101, voting_ends_epoch_millis=10103801,
            description="desc", description_img="/img", author="author"))
        session.add(Election(
            id=83826, name="vote d", status="status", number_winners=1,
            voting_begins_epoch_millis=1010101, voting_ends_epoch_millis=10103801,
            description="desc", description_img="/img", author="author"))

        session.add(Chapter(id=111, name="SF"))

        session.add(Committee(id=2514, name="tech", provisional=False))
        session.add(Committee(id=1564, name="electoral", provisional=False))
        session.add(Committee(id=6362, name="clite", provisional=False))

        session.add(InterestTopic(id=8392, name="interest 1"))
        session.add(InterestTopic(id=9187, name="interest 2"))
        session.add(InterestTopic(id=6452, name="interest 3"))

        session.add(Meeting(
            id=7291, short_id=7291, name="meeting 1", committee_id=2514,
            start_time=datetime(2019, 1, 1), end_time=datetime(2019, 1, 1),
            landing_url="meeting1.com"))
        session.add(Meeting(
            id=42618, short_id=42618, name="meeting 2", committee_id=1564,
            start_time=datetime(2019, 1, 1), end_time=datetime(2019, 1, 1),
            landing_url="meeting2.com"))

        # Setup data modified by merge function
        session.add(Member(
            id=58337, date_created=datetime(2019, 1, 1), first_name="first", last_name="last",
            email_address="email1@gmail.com", normalized_email="email1@gmail.com",
            do_not_call=False, do_not_email=False, biography="bio"))
        session.add(Member(
            id=6278, date_created=datetime(2019, 1, 1), first_name="first", last_name="last",
            email_address="email2@gmail.com", normalized_email="email1@gmail.com",
            do_not_call=False, do_not_email=False, biography="bio"))
        session.add(Member(
            id=928276, date_created=datetime(2019, 1, 1), first_name="first", last_name="last",
            email_address="email3@gmail.com", normalized_email="email1@gmail.com",
            do_not_call=False, do_not_email=False, biography="bio"))

        session.add(Attendee(id=62827, meeting_id=7291, member_id=58337, eligible_to_vote=True))
        session.add(Attendee(id=92729, meeting_id=7291, member_id=6278, eligible_to_vote=True))
        session.add(Attendee(id=52719, meeting_id=42618, member_id=6278, eligible_to_vote=True))
        session.add(Attendee(id=6382892, meeting_id=7291, member_id=928276, eligible_to_vote=True))

        session.add(EligibleVoter(id=74929, member_id=58337, voted=True, election_id=509329))
        session.add(EligibleVoter(id=92827, member_id=58337, voted=False, election_id=62829))
        session.add(EligibleVoter(id=29294, member_id=58337, voted=True, election_id=83826))
        session.add(EligibleVoter(id=28953, member_id=6278, voted=True, election_id=509329))
        session.add(EligibleVoter(id=27271, member_id=6278, voted=True, election_id=62829))
        session.add(EligibleVoter(id=47298, member_id=6278, voted=True, election_id=72920))
        session.add(EligibleVoter(id=59403, member_id=6278, voted=False, election_id=83826))
        session.add(EligibleVoter(id=83729, member_id=928276, voted=True, election_id=509329))

        session.add(Identity(
            id=329, member_id=58337, date_imported=datetime(2019, 1, 1), provider_name="provider 1",
            provider_id=8291))
        session.add(Identity(
            id=1827, member_id=6278, date_imported=datetime(2019, 1, 1), provider_name="provider 2",
            provider_id=3948))
        session.add(Identity(
            id=6382, member_id=928276, date_imported=datetime(2019, 1, 1),
            provider_name="provider 3", provider_id=9271))

        session.add(Interest(
            id=8271, member_id=58337, topic_id=8392, created_date=datetime(2019, 1, 1)))
        session.add(Interest(
            id=1937, member_id=58337, topic_id=9187, created_date=datetime(2019, 1, 5)))
        session.add(Interest(
            id=5627, member_id=6278, topic_id=8392, created_date=datetime(2019, 1, 5)))
        session.add(Interest(
            id=6291, member_id=6278, topic_id=9187, created_date=datetime(2019, 1, 1)))
        session.add(Interest(
            id=8272, member_id=6278, topic_id=6452, created_date=datetime(2019, 1, 1)))
        session.add(Interest(
            id=9389, member_id=928276, topic_id=6452, created_date=datetime(2019, 1, 1)))

        session.add(NationalMembershipData(
            id=5739, date_imported=datetime(2019, 1, 1), active=True, member_id=58337,
            ak_id="ak_id1", dsa_id="sf", do_not_call=False, first_name="first",
            middle_name="middle", last_name="last", address_line_1="addr 1",
            address_line_2="addr 2", city="sf", country="usa", zipcode="94117",
            join_date=datetime(2019, 1, 1), dues_paid_until=datetime(2020, 1, 1)))
        session.add(NationalMembershipData(
            id=7393, date_imported=datetime(2019, 1, 1), active=True, member_id=6278,
            ak_id="ak_id2", dsa_id="sf", do_not_call=False, first_name="first",
            middle_name="middle", last_name="last", address_line_1="addr 1",
            address_line_2="addr 2", city="sf", country="usa", zipcode="94117",
            join_date=datetime(2019, 1, 1), dues_paid_until=datetime(2020, 1, 1)))
        session.add(NationalMembershipData(
            id=2661, date_imported=datetime(2019, 1, 1), active=True, member_id=928276,
            ak_id="ak_id3", dsa_id="sf", do_not_call=False, first_name="first",
            middle_name="middle", last_name="last", address_line_1="addr 1",
            address_line_2="addr 2", city="sf", country="usa", zipcode="94117",
            join_date=datetime(2019, 1, 1), dues_paid_until=datetime(2020, 1, 1)))

        session.add(PhoneNumber(
            id=1738, member_id=58337, date_imported=datetime(2019, 1, 1), number="1234567890",
            name="keep"))
        session.add(PhoneNumber(
            id=8211, member_id=58337, date_imported=datetime(2019, 1, 5), number="4567891234",
            name="update"))
        session.add(PhoneNumber(
            id=9901, member_id=6278, date_imported=datetime(2019, 1, 5), number="1234567890",
            name="delete"))
        session.add(PhoneNumber(
            id=7380, member_id=6278, date_imported=datetime(2019, 1, 1), number="2345678901",
            name="keep"))
        session.add(PhoneNumber(
            id=9999, member_id=6278, date_imported=datetime(2019, 1, 1), number="4567891234",
            name="delete"))
        session.add(PhoneNumber(
            id=2718, member_id=928276, date_imported=datetime(2019, 1, 1), number="3456789012",
            name="keep"))

        session.add(ProxyToken(
            id="7829", member_id=58337, meeting_id=7291, receiving_member_id=928276,
            state=ProxyTokenState.ACCEPTED))
        session.add(ProxyToken(
            id="9388", member_id=6278, meeting_id=7291, receiving_member_id=928276,
            state=ProxyTokenState.ACCEPTED))
        session.add(ProxyToken(
            id="8263", member_id=6278, meeting_id=42618, receiving_member_id=928276,
            state=ProxyTokenState.ACCEPTED))
        session.add(ProxyToken(
            id="7462", member_id=928276, meeting_id=7291, receiving_member_id=6278,
            state=ProxyTokenState.ACCEPTED))
        session.add(ProxyToken(
            id="5352", member_id=928276, meeting_id=7291, receiving_member_id=58337,
            state=ProxyTokenState.ACCEPTED))

        session.add(Role(
            id=3333, chapter_id=111, committee_id=2514, member_id=58337, role="role 1",
            date_created=datetime(2019, 1, 1)))
        session.add(Role(
            id=4444, chapter_id=111, committee_id=2514, member_id=58337, role="role 2",
            date_created=datetime(2019, 1, 5)))
        session.add(Role(
            id=5555, chapter_id=111, committee_id=2514, member_id=6278, role="role 1",
            date_created=datetime(2019, 1, 5)))
        session.add(Role(
            id=6666, chapter_id=111, committee_id=2514, member_id=6278, role="role 2",
            date_created=datetime(2019, 1, 1)))
        session.add(Role(
            id=7777, chapter_id=111, committee_id=2514, member_id=6278, role="role 3",
            date_created=datetime(2019, 1, 1)))
        session.add(Role(
            id=8888, chapter_id=111, committee_id=1564, member_id=6278, role="role 1",
            date_created=datetime(2019, 1, 1)))
        session.add(Role(
            id=9999, chapter_id=111, committee_id=1564, member_id=928276, role="role 1",
            date_created=datetime(2019, 1, 1)))

        session.commit()
        session.close()

        self.response = post_json(
            self.app, '/member/58337/merge?member_to_remove_id=6278&force=True', payload=None)

    def teardown(self):
        metadata.drop_all(engine)

    def test_response(self):
        assert self.response.status_code == 200

    def test_member(self):
        session = Session()

        keep = session.query(Member).filter(Member.id == 58337).first()
        assert keep is not None, ("The record containing the member_id to keep still exists.")

        remove = session.query(Member).filter(Member.id == 6278).first()
        assert remove is None, ("The record containing the member_id to remove no longer exists.")

        control = session.query(Member).filter(Member.id == 928276).first()
        assert control is not None, ("The record not involed in the merge still exists.")

        session.close()

    def test_attendee(self):
        session = Session()

        keep = session.query(Attendee).filter(Attendee.id == 62827).first()
        assert keep is not None, ("The record containing the member_id to keep sould still exist.")

        remove = session.query(Attendee).filter(Attendee.id == 92729).first()
        assert remove is None, \
            ("Record with id 92729 duplicates record with id 62827 and should have been removed.")

        update = session.query(Attendee).filter(Attendee.id == 52719).first()
        assert update is not None, \
            ("Record with id 52719 is not a duplicate and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 52719 should have its member_id updated to the id marked to be kept.")

        control = session.query(Attendee).filter(Attendee.id == 6382892).first()
        assert control is not None, \
            ("The record not involved in the merge should still exist.")

        session.close()

    def test_eligible_voter(self):
        session = Session()

        keep1 = session.query(EligibleVoter).filter(EligibleVoter.member_id == 74929).count()
        assert keep1 is not None, \
            ("The records containing the member_id to keep should still exist.")

        keep2 = session.query(EligibleVoter).filter(EligibleVoter.member_id == 92827).count()
        assert keep2 is not None, \
            ("The records containing the member_id to keep should still exist.")

        keep3 = session.query(EligibleVoter).filter(EligibleVoter.member_id == 29294).count()
        assert keep3 is not None, \
            ("The records containing the member_id to keep should still exist.")

        update1 = session.query(EligibleVoter).filter(EligibleVoter.id == 92827).first()
        assert update1 is not None
        assert update1.voted, \
            ("There was a duplicate for the record containing election_id 62829 that was marked \
              as having voted and was deleted. However, the kept record was marked as not having \
              voted, and should have been updated.")

        remove = session.query(EligibleVoter).filter(EligibleVoter.member_id == 6278).count()
        assert remove == 0, ("The records containing the member_id to remove should not exist.")

        update = session.query(EligibleVoter).filter(EligibleVoter.id == 47298).first()
        assert update is not None, \
            ("Record with id 47298 is not a duplicate and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 47298 should have its member_id updated to the id marked to be kept.")

        control = session.query(EligibleVoter).filter(EligibleVoter.id == 83729).count()
        assert control == 1, \
            ("The records not involved in the merge should still exist.")

        session.close()

    def test_identity(self):
        session = Session()

        keep = session.query(Identity).filter(Identity.id == 329).first()
        assert keep is not None, ("The record containing the member_id to keep should still exist.")

        update = session.query(Identity).filter(Identity.id == 1827).first()
        assert update is not None, \
            ("Record with id 1827 is not a duplicate and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 1827 should have its member_id updated to the id marked to be kept.")

        control = session.query(Identity).filter(Identity.id == 6382).first()
        assert control is not None, ("The record not involved in the merge should still exist.")

        session.close()

    def test_interest(self):
        session = Session()

        keep = session.query(Interest).filter(Interest.id == 8271).first()
        assert keep is not None, \
            ("The record containing the member_id to keep should still exist.")

        update1 = session.query(Interest).filter(Interest.id == 1937).first()
        assert update1 is not None, \
            ("The record containing the member_id to keep should still exist.")
        assert update1.created_date.date() == datetime(2019, 1, 1).date(), \
            ("The duplicate record 6291 has an earlier creation date, so this record should be \
              updated to that date.")

        remove1 = session.query(Interest).filter(Interest.id == 5627).first()
        assert remove1 is None, \
            ("Record with id 5627 duplicates record with id 8271 and should have been removed.")

        remove2 = session.query(Interest).filter(Interest.id == 6291).first()
        assert remove2 is None, \
            ("Record with id 6291 duplicates record with id 1937 and should have been removed.")

        update = session.query(Interest).filter(Interest.id == 8272).first()
        assert update is not None, \
            ("Record with id 8272 is not a duplicate and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 8272 should have its member_id updated to the id marked to be kept.")

        control = session.query(Interest).filter(Interest.id == 9389).first()
        assert control is not None, ("The record not involved in the merge should still exist.")

        session.close()

    def test_national_membership(self):
        session = Session()

        keep = session.query(NationalMembershipData) \
            .filter(NationalMembershipData.id == 5739) \
            .first()
        assert keep is not None, ("The record containing the member_id to keep should still exist.")

        update = session.query(NationalMembershipData) \
            .filter(NationalMembershipData.id == 7393) \
            .first()
        assert update is not None, \
            ("Record with id 7393 is not a duplicate and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 7393 should have its member_id updated to the id marked to be kept.")

        control = session.query(NationalMembershipData) \
            .filter(NationalMembershipData.id == 2661) \
            .first()
        assert control is not None, ("The record not involved in the merge should still exist.")

        session.close()

    def test_phone_number(self):
        session = Session()

        keep = session.query(PhoneNumber).filter(PhoneNumber.id == 1738).first()
        assert keep is not None, ("The record containing the member_id to keep should still exist.")

        update1 = session.query(PhoneNumber).filter(PhoneNumber.id == 8211).first()
        assert update1 is not None, \
            ("The record containing the member_id to keep should still exist.")
        assert update1.date_imported.date() == datetime(2019, 1, 1).date(), \
            ("The duplicate record 8211 has an earlier creation date, so this record should be \
              updated to that date.")

        remove1 = session.query(PhoneNumber).filter(PhoneNumber.id == 9901).first()
        assert remove1 is None, \
            ("Record with id 9901 duplicates record with id 1738 and should have been removed.")

        remove2 = session.query(PhoneNumber).filter(PhoneNumber.id == 9999).first()
        assert remove2 is None, \
            ("Record with id 7380 duplicates record with id 8211 and should have been removed.")

        update = session.query(PhoneNumber).filter(PhoneNumber.id == 7380).first()
        assert update is not None, \
            ("Record with id 9999 is not a duplicate and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 9999 should have its member_id updated to the id marked to be kept.")

        control = session.query(PhoneNumber).filter(PhoneNumber.id == 2718).first()
        assert control is not None, ("The record not involved in the merge should still exist.")

        session.close()

    def test_proxy_token(self):
        session = Session()

        keep = session.query(ProxyToken).filter(ProxyToken.id == "7829").first()
        assert keep is not None, ("The record containing the member_id to keep should still exist.")

        remove = session.query(ProxyToken).filter(ProxyToken.id == "9388").first()
        assert remove is None, \
            ("Record with id 9388 duplicates record with id 7829 and should have been removed.")

        update1 = session.query(ProxyToken).filter(ProxyToken.id == "8263").first()
        assert update1 is not None, \
            ("Record with id 8263 is not a duplicate and should still exist.")
        assert update1.member_id == 58337, \
            ("Record with id 8263 should have its member_id updated to the id marked to be kept.")

        update2 = session.query(ProxyToken).filter(ProxyToken.id == "7462").first()
        assert update2 is not None, \
            ("Record with id 7462 is not involved in the merge and should still exist.")
        assert update2.receiving_member_id == 58337, \
            ("However, it's receiving_member_id refers to the member_id which is being removed and \
              should be updated to the id being kept.")

        control = session.query(ProxyToken).filter(ProxyToken.id == "5352").first()
        assert control is not None, ("The record not involved in the merge should still exist.")

        session.close()

    def test_role(self):
        session = Session()

        keep = session.query(Role).filter(Role.id == 3333).first()
        assert keep is not None, ("The record containing the member_id to keep should still exist.")

        update1 = session.query(Role).filter(Role.id == 4444).first()
        assert update1 is not None, \
            ("The record containing the member_id to keep should still exist.")
        assert update1.date_created.date() == datetime(2019, 1, 1).date(), \
            ("The duplicate record 6666 has an earlier creation date, so this record should be \
              updated to that date.")

        remove1 = session.query(Role).filter(Role.id == 5555).first()
        assert remove1 is None, \
            ("Record with id 5555 duplicates record with id 3333 and should have been removed.")

        remove2 = session.query(Role).filter(Role.id == 6666).first()
        assert remove2 is None, \
            ("Record with id 6666 duplicates record with id 4444 and should have been removed.")

        update = session.query(Role).filter(Role.id == 7777).first()
        assert update is not None, \
            ("Record with id 7777 is not a duplicate due to having a unique committee, role pair \
              and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 7777 should have its member_id updated to the id marked to be kept.")

        update = session.query(Role).filter(Role.id == 8888).first()
        assert update is not None, \
            ("Record with id 8888 is not a duplicate due to having a unique committee, role pair \
              and should still exist.")
        assert update.member_id == 58337, \
            ("Record with id 8888 should have its member_id updated to the id marked to be kept.")

        control = session.query(Role).filter(Role.id == 9999).first()
        assert control is not None, ("The record not involved in the merge should still exist.")

        session.close()
