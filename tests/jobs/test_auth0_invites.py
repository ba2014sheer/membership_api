import io
from typing import IO
from unittest.mock import Mock

from config import SUPER_USER_EMAIL, SUPER_USER_FIRST_NAME, SUPER_USER_LAST_NAME
from jobs.auth0_invites import SendAuth0Invites
from membership.database.base import engine, metadata, Session
from membership.database.models import Member


class NullIO(io.StringIO):
    def write(self, *args):
        pass


devnull: IO[str] = NullIO()


class TestSendAuth0Invites:
    job = SendAuth0Invites()
    job.send_invite = Mock()

    def setup(self):
        metadata.create_all(engine)

        session = Session()
        # set up for auth
        member = Member(
            first_name=SUPER_USER_FIRST_NAME,
            last_name=SUPER_USER_LAST_NAME,
            email_address=SUPER_USER_EMAIL,
        )
        session.add(member)

        session.commit()
        session.close()

    def teardown(self):
        metadata.drop_all(engine)

    def test_find_existing_member_by_email(self):
        session = Session()
        member = session.query(Member).get(1)
        self.job.send_invites([member.id], out=devnull)
