from membership.database.models import Member, Attendee, EligibleVoter, Identity, Interest, \
    PhoneNumber, ProxyToken, Role
from typing import List


class MergeMemberFetchResult:

    def __init__(self,
                 member_to_keep: Member,
                 member_to_remove: Member,
                 attendees_to_keep: List[Attendee],
                 attendees_to_remove: List[Attendee],
                 eligible_voters_to_keep: List[EligibleVoter],
                 eligible_voters_to_remove: List[EligibleVoter],
                 identities_to_keep: List[Identity],
                 identities_to_remove: List[Identity],
                 interests_to_keep: List[Interest],
                 interests_to_remove: List[Interest],
                 national_members_to_keep: List[Interest],
                 national_members_to_remove: List[Interest],
                 phone_numbers_to_keep: List[PhoneNumber],
                 phone_numbers_to_remove: List[PhoneNumber],
                 proxy_tokens_to_keep: List[ProxyToken],
                 proxy_tokens_to_remove: List[ProxyToken],
                 additional_proxy_tokens_to_update: List[ProxyToken],
                 roles_to_keep: List[Role],
                 roles_to_remove: List[Role]):
        self.member_to_keep = member_to_keep
        self.member_to_remove = member_to_remove
        self.attendees_to_keep = attendees_to_keep
        self.attendees_to_remove = attendees_to_remove
        self.eligible_voters_to_keep = eligible_voters_to_keep
        self.eligible_voters_to_remove = eligible_voters_to_remove
        self.identities_to_keep = identities_to_keep
        self.identities_to_remove = identities_to_remove
        self.interests_to_keep = interests_to_keep
        self.interests_to_remove = interests_to_remove
        self.national_members_to_keep = national_members_to_keep
        self.national_members_to_remove = national_members_to_remove
        self.phone_numbers_to_keep = phone_numbers_to_keep
        self.phone_numbers_to_remove = phone_numbers_to_remove
        self.proxy_tokens_to_keep = proxy_tokens_to_keep
        self.proxy_tokens_to_remove = proxy_tokens_to_remove
        self.additional_proxy_tokens_to_update = additional_proxy_tokens_to_update
        self.roles_to_keep = roles_to_keep
        self.roles_to_remove = roles_to_remove
