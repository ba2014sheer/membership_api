from membership.database.models import Election
from membership.schemas import JsonObj
from membership.services import ElectionService

election_service = ElectionService()


def format_election(election: Election) -> JsonObj:
    return {
        'id': election.id,
        'name': election.name,
        'status': election.status,
        'voting_begins_epoch_millis': election.voting_begins_epoch_millis,
        'voting_ends_epoch_millis': election.voting_ends_epoch_millis
    }


def format_election_details(election: Election) -> JsonObj:
    return {
        **(format_election(election)),
        'number_winners': election.number_winners,
        'candidates': [
            {
                'id': candidate.id,
                'name': candidate.name,
                'image_url': candidate.image_url,
            } for candidate in election.candidates
        ],
        'sponsors': [
            {
                'id': sponsor.id,
                'name': sponsor.name,
            } for sponsor in election.sponsors
        ],
        'transitions': election_service.next_election_transitions(election),
        'description': election.description,
        'description_img': election.description_img,
        'author': election.author,
    }
