from Cryptodome.Cipher import AES
from Cryptodome.Cipher._mode_eax import EaxMode
from Cryptodome.Random import get_random_bytes
from base64 import b64encode, b64decode
from json import JSONDecoder, JSONEncoder
from typing import AnyStr, Callable, Generic, Optional, TypeVar, Type, Union

__all__ = [
    'Codec',
    'Decoder',
    'Encoder',
    'Encrypted',
    'JsonLike',
    'Key',
    'create_key',
    'encrypt',
    'key_size',
    'parse',
]

T = TypeVar('T')
Key = AnyStr
JsonLike = TypeVar('JsonLike', None, dict, list, str, int, float, bool)


def create_key(byte_size: int = 32, validate: bool = True) -> Key:
    """
    Creates a str key of the given size (assuming it is a valid sized key)

    :param byte_size: the size of the encryption key in bytes
    :param validate: if True, check that the byte size is valid for an AES encryption key
    :return: a Base64 encoded / ASCII `str` containing the encryption key bytes
    """
    if validate:
        assert byte_size == 32 or byte_size == 16, 'byte_size must be 16 or 32 to be valid for AES'
    return b64encode(get_random_bytes(byte_size)).decode('ascii')


def _decode_encrypt_key(key: Key) -> bytes:
    """
    Decodes the given key as Base64 given bytes or an ASCII encoded `str`
    """
    return b64decode(key)


def _cipher(key: AnyStr, nonce: Optional[bytes] = None) -> EaxMode:
    """
    Creates a PyCryptodomeX AES symmetric key encryption cipher.

    :param key: the encryption key for the cipher
    :param nonce: a nonce for decrypting (only needed for decryption)
    :return: a fresh new cipher (must be regenerated before every attempt to decrypt / encrypt)
    """
    raw_key = _decode_encrypt_key(key)
    return AES.new(raw_key, AES.MODE_EAX, nonce)


# Cache the default decoders
_default_decoder = JSONDecoder()
_default_encoder = JSONEncoder()


def _name(name_or_type: Union[Type[T], str]) -> str:
    return name_or_type.__name__ if isinstance(name_or_type, type) else name_or_type


class Decoder(Generic[T]):
    """
    Decodes a given type from bytes.
    """
    def __init__(self, name_or_type: Union[Type[T], str], decode: Callable[[bytes], T]):
        self.type_name = _name(name_or_type)
        self._decode = decode

    def decode(self, value: bytes) -> T:
        return self._decode(value)

    def __str__(self):
        return f"Decoder[{self.type_name}]"

    def __repr__(self):
        return f"Decoder({repr(self.type_name)}, {repr(self._decode)})"


class Encoder(Generic[T]):
    """
    Encodes a given type to bytes.
    """
    def __init__(self, name_or_type: Union[Type[T], str], encode: Callable[[T], bytes]):
        self.type_name = _name(name_or_type)
        self._encode = encode

    def encode(self, value: T) -> bytes:
        return self._encode(value)

    def __str__(self):
        return f"Encoder[{self.type_name}]"

    def __repr__(self):
        return f"Encoder({repr(self.type_name)}, {repr(self._encode)})"


class Codec(Decoder[T], Encoder[T]):
    """
    Decodes / encodes a given type from / to bytes.
    """
    def __init__(
            self,
            name_or_type: Union[Type[T], str],
            decode: Union[Decoder[T], Callable[[bytes], T]],
            encode: Union[Encoder[T], Callable[[T], bytes]]):
        Decoder.__init__(
            self,
            name_or_type,
            decode.decode if isinstance(decode, Decoder) else decode
        )
        Encoder.__init__(
            self,
            name_or_type,
            encode.encode if isinstance(encode, Encoder) else encode
        )

    def __str__(self):
        return f"Codec[{self.type_name}]"

    def __repr__(self):
        return f"Codec({repr(self.type_name)}, {repr(self._encode)}, {repr(self._decode)})"

    @staticmethod
    def json(
            name_or_type: Union[Type[T], str] = 'JsonLike',
            json_decoder: JSONDecoder = _default_decoder,
            json_encoder: JSONEncoder = _default_encoder,
            str_encoding: str = 'utf8') -> 'Codec[T]':
        """
        Returns a Codec for `JsonLike` python data types.

        :param name_or_type: provide this to specify the shape of the data type,
                             otherwise, assume generic python's `JsonLike` data types
        :param json_decoder: a standard library decoder
        :param json_encoder: a standard library encoder
        :param str_encoding: the character set for(de/en)coding the str result of json (de/en)coding
        :return: a Codec that uses json encoder / decoder to serialize an object for encryption
        """

        def decode(ciphertext: bytes) -> JsonLike:
            return json_decoder.decode(ciphertext.decode(str_encoding))

        def encode(obj: JsonLike) -> bytes:
            return json_encoder.encode(obj).encode(str_encoding)

        return Codec(name_or_type, decode, encode)

    @staticmethod
    def obj(
            value_name_or_type: Union[Type[T], str] = 'JsonLike',
            json_decoder: JSONDecoder = _default_decoder,
            json_encoder: JSONEncoder = _default_encoder,
            str_encoding: str = 'utf8') -> 'Codec[T]':
        """
        Returns a Codec for root level JSON object like python dictionaries.

        See `Codec.json()` above
        """
        return Codec.json(
            f"Dict[str, {_name(value_name_or_type)}]",
            json_decoder,
            json_encoder,
            str_encoding
        )


class Encrypted(Generic[T]):
    """
    Hold an encrypted value in memory of a given type as a string of bytes.

    Useful for for safe-ish transport in memory with less assumptions about how to decrypt
    the underlying bytes. Since the encryption key is in scope everywhere in the code,
    there is little benefit for trying to hide encryption keys in memory. This class
    just ensures that we don't accidentally log a `str()` version of the encryption key.

    :param ciphertext: the slice of bytes holding the ciphertext (aka encrypted message)
    :param nonce: an number used once (nonce) code to ensure that encrypting the same message
                  always results in a different ciphertext
    :param mac: the machine authentication code for cipher block chaining
    :param decoder: captured during construction to provide generic delayed decryption
                    (you can always call `.decrypt()` for)
    :param encrypt_key: captures the key used to encrypt the data, so that it can be
                        decrypted later. It will not be in the `x.serialized` or `str(x)` value.
    """

    def __init__(
            self,
            ciphertext: bytes,
            nonce: bytes,
            mac: bytes,
            decoder: Decoder[T],
            encrypt_key: Key):
        self.ciphertext = ciphertext
        self.mac = mac
        self.nonce = nonce
        self._decoder = decoder
        self._serialized = None

        def decrypt_internal():
            cipher = _cipher(encrypt_key, nonce=self.nonce)
            return cipher.decrypt_and_verify(self.ciphertext, self.mac)

        def serialize_internal():
            return b64encode(self.nonce + self.mac + self.ciphertext)

        self._decrypt_internal = decrypt_internal
        self._serialize_internal = serialize_internal

    def __eq__(self, other):
        raise TypeError("Encrypted values should never be used for comparison")

    def decrypt(self) -> T:
        """
        Decrypts the value with the captured encryption key and decoder.
        """
        return self._decoder.decode(self._decrypt_internal())

    @property
    def serialized(self) -> bytes:
        """
        Serializes the encrypted value as Base64 bytes.

        :return: Base64 encoded bytes
        """
        if self._serialized is None:
            self._serialized = self._serialize_internal()
        return self._serialized

    def __str__(self):
        """
        Serialize the insides encrypted wrapper without hiding that it is a wrapped value.

        If you just want the inner bytes, use `.serialized`

        :return: a string representation of the Base64 encrypted bytes stored in this value
        """
        return f"Encrypted[{self._decoder.type_name}]({self.serialized})"

    def __repr__(self):
        return str(self)


def encrypt(value: T, key: Key, codec: Codec[T] = Codec.json()) -> Encrypted[T]:
    """
    Encrypt the given value using the provided codec to encode the value, then encrypt the value
    with the given encryption key, and serialize all of this into an Encrypted object.

    :param value: the unencrypted JsonLike object (or any object given the appropriate codec)
    :param codec: a Codec that can both encode the given value and decode / invert the encoding
    :param key: the encryption key to use to encrypt the encoded string, storing the relevant bytes
    :return: an Encrypted wrapper of the given type of value
    """
    c = _cipher(key)
    plainbytes = codec.encode(value)
    cipherbytes, mac = c.encrypt_and_digest(plainbytes)
    return Encrypted(cipherbytes, c.nonce, mac, codec, key)


def key_size(key: Key) -> int:
    """
    Measure the size of a given encryption key in bytes.

    :param key: the encryption key as base64 encoded `bytes` or ASCII encoded `str`
    :return: the length of the encryption key in bytes when decoded
    """
    return len(_decode_encrypt_key(key))


def parse(serialized: bytes, key: Key, decoder: Decoder[T] = Codec.json()) -> Encrypted[T]:
    """
    Parses a given string as an encrypted value with its specific decoder function to simplify
    generic delayed decryption.

    For a Codec[List[int]], you can use:

    - `c = Codec.json(list)` for an approximation of `Codec[list]`
    - `c: Codec[List[int]] = Codec.json('List[int]'))` for a more type descriptive codec

    :param serialized: the raw encrypted bytes as serialized (assumes utf-8 when given `str`)
    :param decoder: decodes the decrypted bytes as an instance of an expected type
    :param key: the encryption key used to store for later decryption
    :return: the `Encrypted` wrapped object
    """
    decoded_bytes: bytes = b64decode(serialized)
    return Encrypted(
        nonce=decoded_bytes[:16],
        mac=decoded_bytes[16:32],
        ciphertext=decoded_bytes[32:],
        decoder=decoder,
        encrypt_key=key,
    )
