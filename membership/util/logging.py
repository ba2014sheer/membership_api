import logging
from logging import Logger
from types import TracebackType
from typing import AnyStr, IO, Iterable, List, Optional, Type, Iterator


class LoggerIO(IO):

    def __init__(self, level: int, logger: Logger = logging):
        self.level = level
        self._logger = logger
        self._msg = ''

    def write(self, message: AnyStr):
        self._msg = self._msg + message
        while '\n' in self._msg:
            pos = self._msg.find('\n')
            self._logger.log(self.level, self._msg[:pos])
            self._msg = self._msg[pos + 1:]

    def flush(self):
        if self._msg != '':
            self._logger.log(self.level, self._msg)
            self._msg = ''

    def writable(self) -> bool:
        return True

    def writelines(self, lines: Iterable[AnyStr]) -> None:
        for line in lines:
            self.write(line + '\n')

    def __enter__(self) -> IO[AnyStr]:
        return self

    def __exit__(
            self,
            t: Optional[Type[BaseException]],
            value: Optional[BaseException],
            traceback: Optional[TracebackType]
    ) -> bool:
        pass

    def __next__(self) -> AnyStr:
        pass

    def __iter__(self) -> Iterator[AnyStr]:
        pass

    def close(self) -> None:
        pass

    def fileno(self) -> int:
        pass

    def isatty(self) -> bool:
        pass

    def read(self, n: int = ...) -> AnyStr:
        pass

    def readable(self) -> bool:
        pass

    def readline(self, limit: int = ...) -> AnyStr:
        pass

    def readlines(self, hint: int = ...) -> List[AnyStr]:
        pass

    def seek(self, offset: int, whence: int = ...) -> int:
        pass

    def seekable(self) -> bool:
        pass

    def tell(self) -> int:
        pass

    def truncate(self, size: Optional[int] = ...) -> int:
        pass

    closed = property(lambda self: object(), lambda self, v: None, lambda self: None)

    mode = property(lambda self: object(), lambda self, v: None, lambda self: None)

    name = property(lambda self: object(), lambda self, v: None, lambda self: None)

    line_buffering = property(lambda self: object(), lambda self, v: None, lambda self: None)

    newlines = property(lambda self: object(), lambda self, v: None, lambda self: None)
