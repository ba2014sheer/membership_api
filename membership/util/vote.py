import random
from decimal import Decimal, getcontext
from enum import Enum
from typing import Any, Dict, Generic, List, Set, TypeVar  # noqa: F401

ZERO = Decimal('0.00000')
ONE = Decimal('1.00000')

T = TypeVar('T')


class Vote(Generic[T]):
    def __init__(self, choices: List[T]):
        self.weight: Decimal = ONE
        self.choice_stack: List[T] = list(reversed(choices))

    def transfer(self, transfer_weight: Decimal, remaining_candidates: Set[T]) -> None:
        self.weight *= transfer_weight
        self.choice_stack.pop()
        while self.choice_stack and self.choice_stack[-1] not in remaining_candidates:
            self.choice_stack.pop()


class CandidateVotes(Generic[T]):
    def __init__(self, candidate: T):
        self.candidate: T = candidate
        self.total: Decimal = ZERO
        self.votes: List[Vote[T]] = []


class ElectionWinner(Generic[T]):
    def __init__(self, candidate: T, round: int):
        self.candidate: T = candidate
        self.round: int = round

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, self.__class__):
            return self.candidate == other.candidate and self.round == other.round
        else:
            return False

    def __repr__(self) -> str:
        return 'winner: {}, round: {}'.format(self.candidate, self.round)


class CandidateVoteCounts:
    def __init__(self, starting: Decimal = ZERO, received: Decimal = ZERO):
        self.starting: Decimal = starting
        self.received: Decimal = received

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, self.__class__):
            return self.starting == other.starting and self.received == other.received
        else:
            return False

    def __repr__(self) -> str:
        return 'starting: {}, received: {}'.format(self.starting, self.received)


class VoteTransferType(Enum):
    EXCESS = 'excess'
    ELIMINATION = 'elimination'


class ElectionRound(Generic[T]):
    def __init__(self,
                 transfer_type: VoteTransferType = None,
                 transfer_source: T = None,
                 vote_counts: Dict[T, CandidateVoteCounts] = {}):
        self.transfer_type: VoteTransferType = transfer_type
        self.transfer_source: T = transfer_source
        self.vote_counts: Dict[T, CandidateVoteCounts] = vote_counts

    def __eq__(self, other: Any) -> bool:
        if isinstance(other, self.__class__):
            return self.transfer_type == other.transfer_type and \
                   self.transfer_source == other.transfer_source and \
                   self.vote_counts == other.vote_counts
        else:
            return False


class STVElection(Generic[T]):
    def __init__(self,
                 candidates: List[T],
                 num_winners: int,
                 choices_list: List[List[T]],
                 seed: int):
        self.votes: List[Vote[T]] = [Vote(choices) for choices in choices_list]
        self.winners: List[ElectionWinner] = []
        self.remaining_candidates: List[T] = list(candidates)
        self.num_winners: int = num_winners
        self.rounds: List[ElectionRound[T]] = []
        self.random: random = random.Random(seed)
        self.quota = int(len(self.votes) / (self.num_winners + 1)) + 1
        getcontext().prec = 5

    def hold_election(self) -> List[T]:
        while len(self.winners) < self.num_winners and len(self.remaining_candidates) > 0:
            self.count_votes()
        return self.winners

    def count_votes(self) -> None:
        candidate_votes: Dict[T, CandidateVotes[T]] = {
            candidate: CandidateVotes(candidate) for candidate in self.remaining_candidates
        }

        # Tally votes by candidate
        for vote in self.votes:  # type: Vote[T]
            if len(vote.choice_stack) > 0 and vote.weight > ZERO:
                cv = candidate_votes[vote.choice_stack[-1]]
                cv.total += vote.weight
                cv.votes.append(vote)

        # Sort candidates by vote total
        result = list(candidate_votes.values())
        result.sort(key=lambda x: x.total, reverse=True)

        # Record the starting vote totals for the round
        round = ElectionRound()
        round.vote_counts = {
            cv.candidate: CandidateVoteCounts(starting=cv.total)
            for cv in result
        }
        self.rounds.append(round)
        round_number = len(self.rounds)

        if result[0].total >= self.quota or \
           len(self.remaining_candidates) <= self.num_winners - len(self.winners):
            # Choose a round winner from the candidates with more votes than the quota
            i = 0
            round_winners = []
            while i < len(result) and result[i].total == result[0].total:
                round_winners.append(result[i])
                i += 1
            winner = self.break_tie(round_winners, round_number - 1, True)
            self.winners.append(ElectionWinner(winner.candidate, round_number))
            if winner.total > self.quota:
                transfer_weight = Decimal((winner.total - self.quota) / winner.total).quantize(ZERO)
            else:
                transfer_weight = ZERO
            self.transfer_votes(winner, transfer_weight, VoteTransferType.EXCESS, round)
        else:
            # Eliminate the candidate with the fewest votes
            i = len(result) - 1
            round_losers = []
            while i >= 0 and result[i].total == result[-1].total:
                round_losers.append(result[i])
                i -= 1
            loser = self.break_tie(round_losers, round_number - 1, False)
            self.transfer_votes(loser, ONE, VoteTransferType.ELIMINATION, round)

    def break_tie(self,
                  round_winners: List[CandidateVotes[T]],
                  voting_round: int,
                  win: bool) -> CandidateVotes[T]:
        multiplier = 1 if win else -1
        if len(round_winners) == 1:
            return round_winners[0]
        if voting_round == 0:
            return self.random.choice(round_winners)
        max_vote = None
        next_round_winners = []
        for w in round_winners:
            total = multiplier * self.rounds[voting_round - 1].vote_counts[w.candidate].starting
            if max_vote is None or total > max_vote:
                next_round_winners = [w]
                max_vote = total
            elif total == max_vote:
                next_round_winners.append(w)
        return self.break_tie(next_round_winners, voting_round - 1, win)

    def transfer_votes(self,
                       candidate_votes: CandidateVotes[T],
                       transfer_weight: Decimal,
                       transfer_type: VoteTransferType,
                       round: ElectionRound) -> None:
        self.remaining_candidates.remove(candidate_votes.candidate)
        round.transfer_type = transfer_type
        round.transfer_source = candidate_votes.candidate
        for vote in candidate_votes.votes:
            vote.transfer(transfer_weight, self.remaining_candidates)
            if len(vote.choice_stack) > 0 and vote.weight > ZERO:
                transfer_recipient = vote.choice_stack[-1]
                round.vote_counts[transfer_recipient].received += vote.weight
