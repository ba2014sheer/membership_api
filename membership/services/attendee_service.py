from typing import Union, Optional

from membership.database.base import Session
from membership.database.models import Attendee, Member, Meeting
from membership.services.errors import ValidationError


class AttendeeService:

    class ErrorCodes:
        ALREADY_ATTENDED = "ALREADY_ATTENDED"

    def attend_meeting_with_short_id(
        self,
        member: Union[Member, int, str],
        short_id: int,
        session: Session
    ) -> Optional[Meeting]:
        meeting: Meeting = session.query(Meeting).filter_by(short_id=short_id).one_or_none()

        if not meeting:
            return None

        try:
            self.attend_meeting(member, meeting, session)
        except ValidationError as e:
            if e.key == AttendeeService.ErrorCodes.ALREADY_ATTENDED:
                raise ValidationError(
                    AttendeeService.ErrorCodes.ALREADY_ATTENDED,
                    "Member has already attended meeting (meeting code: {})".format(short_id)
                )
            else:
                raise e

        return meeting

    def attend_meeting(
        self,
        member: Union[Member, int, str],
        meeting: Union[Meeting, int, str],
        session: Session,
        return_none_on_error: bool = False
    ) -> Optional[Meeting]:
        if type(member) in [int, str]:
            member: Member = session.query(Member).get(member)

        if type(meeting) in [int, str]:
            meeting: Meeting = session.query(Meeting).get(meeting)

        if session.query(Attendee).filter_by(meeting=meeting, member=member).count() == 0:
            attendee = Attendee(meeting=meeting, member=member)
            session.add(attendee)
            session.commit()
        elif return_none_on_error:
            return None
        else:
            raise ValidationError(
                AttendeeService.ErrorCodes.ALREADY_ATTENDED,
                "Member has already attended meeting (id: {})".format(meeting.id)
            )

        return meeting

    def retrieve_attendee(
        self,
        session: Session,
        member: Optional[Union[Member, int, str]] = None,
        meeting: Optional[Union[Meeting, int, str]] = None,
        attendee: Optional[Union[Attendee, int, str]] = None
    ) -> Attendee:
        if type(member) in [int, str]:
            member: Member = session.query(Member).get(member)

        if type(meeting) in [int, str]:
            meeting: Meeting = session.query(Meeting).get(meeting)

        if type(attendee) in [int, str]:
            attendee: Attendee = session.query(Attendee).get(attendee)

        if attendee == None:
            attendee = session.query(Attendee).filter_by(member=member, meeting=meeting).one()
        elif member != None or meeting != None:
            raise ValueError('Must provide a Member and Meeting, or an Attendee')

        return attendee
